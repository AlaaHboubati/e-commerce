const { Console } = require('console');
const path = require('path');
const express = require('express');
const bodyParser = require('body-parser');
const session = require('express-session');
const MongoDbStore = require('connect-mongodb-session')(session);
const User = require('./models/user');
const csrf = require('csurf');
const flash = require('connect-flash');
const multer = require('multer');
const adminRoutes = require('./routes/admin');
const shopRoutes = require('./routes/shop');
const authRoutes = require('./routes/auth');
const errorController = require('./controllers/error');
const mongoose = require('mongoose');
const { collection } = require('./models/user');

csrfProtection = csrf();

const MONGODB_URI = 'mongodb+srv://alaaHboubati:12345alaa@cluster0.ptll8.mongodb.net/shop?retryWrites=true&w=majority';
const app = express();
const store = new MongoDbStore({
   uri: MONGODB_URI,
   collection:'sessions'
});

const fileFilter = (req,file,cb)=>{
    if(file.mimetype === 'image/jpeg' || file.mimetype === 'image/jpg'  ||file.mimetype === 'image/jpeg' ){
        cb(null,true);
    }else{
        cb(null,false);
    }
}
const fileStorage = multer.diskStorage({
    destination:(req,file,cb)=>{
        cb(null,'images');
    },
    filename:(req,file,cb)=>{
        cb(null,Date.now() + '-' + file.originalname);
    }
});


app.use(bodyParser.urlencoded({extended:false}));
app.use(multer({storage:fileStorage,fileFilter:fileFilter}).single('image'));
app.use(express.static(path.join(__dirname,'public')));
app.use('/images',express.static(path.join(__dirname,'images')));

app.use(session({
    secret:'my secret',
    resave:false,
    saveUninitialized:false,
    store:store
}));

app.use(csrfProtection);
app.use((req,res,next)=>{
    res.locals.isAuthenticated = req.session.isLoggedIn;
    res.locals.csrfToken = req.csrfToken();
    next();
})

app.get('/500',errorController.get500);

app.use((req,res,next)=>{
    if(!req.session.user){
        return next();
    }
    User.findById(req.session.user._id)
    .then(user=>{
        if(!user){
            return next();
        }
        req.user = user;
        next();
    })
    .catch(err=>{
        return errorController.handleError(err,next);
    });
})

app.use(flash());
app.use('/admin',adminRoutes);
app.use(shopRoutes);
app.use(authRoutes);


app.set('view engine','ejs');
app.set('views','views');
app.use(errorController.get404);
app.use((error,req,res,next)=>{
    res.redirect('/500');
})
mongoose.connect(MONGODB_URI)
.then((result)=>{
    console.log("Connected!");
    app.listen(3000);
}).catch(err=>{
    console.log(err);
});

