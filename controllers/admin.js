const Product = require('../models/product');
const {validationResult} = require('express-validator');
const mongoose = require('mongoose');
const product = require('../models/product');
const fileHelper = require('../util/fileHelper');
const errorController = require('./error');
exports.getAddProduct = (req,res,next)=>{

    res.render('admin/edit-product',{
        pageTitle:'Add Product',
        path:'/admin/add-product',
        editing:false,
        hasError:false,
        errorMessage:null,
        validationErrors:[]
    });
}
exports.postAddProduct= (req,res,next)=>{
    const title = req.body.title;
    const image = req.file;
    const price = req.body.price;
    const description = req.body.description;
    const errors = validationResult(req);
    if(!errors.isEmpty() || !image){
        return res.status(420).render('admin/edit-product',{
            pageTitle:'Add Product',
            path:'/admin/add-product',
            editing:false ,
            hasError:true,
            errorMessage:image?errors.array()[0].msg:'Attached file is not an image.',
            product:{
                title:title,
                price:price,
                description:description
            },
            validationErrors:image?errors.array():[]
        });
    }
    const imageUrl = image.path;
    const product = new Product({
        title:title,
        imageUrl:imageUrl,
        price:price,
        description:description,
        userId:req.user._id
    });
    product.save()
    .then((result)=>{
        console.log(result);
        res.redirect('/admin/products');
    }).catch(err=>{
        const error = new Error(err);
        error.httpStatusCode = 500;
        return next(error);
        });
}

exports.getEditProduct = (req,res,next)=>{
    const editMode = req.query.edit;
    const productId = req.params.productId;
    Product.findById(productId)
    .then(product =>{
        if(!product){
            return res.redirect('/');
        }
        res.render('admin/edit-product',{
            pageTitle:'Edit Product',
            path:'/admin/edit-product',
            editing:editMode ,
            hasError:false,
            errorMessage:null,
            product:product,
            validationErrors:[]
        });
    }).catch(err=>{
        return errorController.handleError(err,next);
    });
}

exports.postEditProduct = (req,res,next)=>{
    const productId = req.body.productId;
    const updatedTitle = req.body.title;
    const image = req.file;
    const updatedPrice = req.body.price;
    const updatedDesc = req.body.description;
    const errors = validationResult(req);
    if(!errors.isEmpty()){
        return res.status(420).render('admin/edit-product',{
            pageTitle:'Edit Product',
            path:'/admin/edit-product',
            editing:true ,
            hasError:true,
            errorMessage:errors.array()[0].msg,
            product:{
                title:updatedTitle,
                price:updatedPrice,
                description:updatedDesc,
                _id:productId
            },
            validationErrors:errors.array()
        });
    }
    Product.findById(mongoose.Types.ObjectId(productId))
    .then(product=>{
        if(product.userId.toString() !== req.user._id.toString()){
            res.redirect('/');
        }
        product.title = updatedTitle;
        if(image){
            fileHelper.deleteFile(product.imageUrl);
            product.imageUrl = image.path;
        }
        product.price = updatedPrice;
        product.description = updatedDesc;
        return product.save();
    }).then(result=>{
     console.log("Product Updated");
     res.redirect('/admin/products');
    }).catch(err=>{
        return errorController.handleError(err,next);
    });
}

exports.deleteProduct = (req,res,next)=>{
    const productId = req.params.productId;
    Product.findById(productId)
    .then(product=>{
        if(!product){
            return next(new Error('Product not found.'));
        }
        fileHelper.deleteFile(product.imageUrl);
        return  Product.deleteOne({
            _id:productId,
            userId:req.user._id
        });
    })
    .then(result=>{
        console.log("Product Deleted");
        res.status(200).json({message:"success!"})
    }).catch(err=>{
        res.status(200).json({message:"failed!"})
    });
}

exports.getProducts = (req,res,next) => {
    Product.find({
        userId:mongoose.Types.ObjectId(req.user._id)
    })
    .then(products=>{
        res.render
        ('admin/products',
        {
            prods:products,
            pageTitle:'Admin Products',
            path:'/admin/products',
        }
        );
    }).catch(err=>{
        return errorController.handleError(err,next);
    });
}
