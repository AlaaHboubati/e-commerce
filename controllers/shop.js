const Product = require('../models/product');
const User = require('../models/user');
const Order = require('../models/order');
const errorController = require('./error')
const fs = require('fs');
const path = require('path');
const PDFDocument = require('pdfkit');
const mongoose = require('mongoose');

const ITEMS_PER_PAGE = 1;


exports.getProducts = (req,res,next) => {
    const page = +req.query.page || 1;
    let totalItems;
    Product.find()
    .countDocuments()
    .then(numOfProducts =>{
        totalItems = numOfProducts;
        return  Product.find()
                    .skip((page-1)*ITEMS_PER_PAGE)
                    .limit(ITEMS_PER_PAGE)
    })
    .then(products=>{
        res.render
        ('shop/product-list',
        {
            prods:products,
            pageTitle:'Prodcts',
            path:'/products',
            hasNextPage:ITEMS_PER_PAGE * page < totalItems,
            hasPreviousPage:page > 1,
            currentPage:page,
            nextPage:page+1,
            previousPage:page-1,
            lastPage:Math.ceil(totalItems/ITEMS_PER_PAGE)
        }
        );
    }).catch(err=>{
        console.log(err);
    });
    
    
}
exports.getProduct = (req,res,next)=>{
    const id = req.params.productId;
    Product.findById(id)
    .then(prod =>{
        res.render('shop/product-detail',{
            product:prod,
            pageTitle:prod.title,
            path:"/products",

        });
    }).catch(err=>{
        console.log(err);
    })
}

exports.getIndex = (req,res,next) => {
    const page = +req.query.page || 1;
    let totalItems;
    Product.find()
    .countDocuments()
    .then(numOfProducts =>{
        totalItems = numOfProducts;
        return  Product.find()
                    .skip((page-1)*ITEMS_PER_PAGE)
                    .limit(ITEMS_PER_PAGE)
    })
    .then(products=>{
        res.render
        ('shop/index',
        {
            prods:products,
            pageTitle:'HomePage',
            path:'/',
            hasNextPage:ITEMS_PER_PAGE * page < totalItems,
            hasPreviousPage:page > 1,
            currentPage:page,
            nextPage:page+1,
            previousPage:page-1,
            lastPage:Math.ceil(totalItems/ITEMS_PER_PAGE)
        }
        );
    }).catch(err=>{
        console.log(err);
    });
}

exports.getCart = (req,res,next) => {
        req.user
        .populate('cart.items.productId')
        .execPopulate()
        .then(user=>{
            const products = user.cart.items;
            res.render('shop/cart',
            {
                pageTitle:'Your Cart',
                path:'/cart',
                products:products,
            });  
        });
                    
}
exports.postCart = (req,res,next)=>{
    const productId = req.body.productId;
    Product.findById(productId)
    .then(product=>{
        req.user.addToCart(product);
        res.redirect('/cart');
    })
    .catch(err=>{
        return errorController.handleError(err,next);
    })
  
}

exports.postCartDeleteProduct = (req,res,next)=>{
    const prodId = req.body.productId;
    req.user.removeFromCart(prodId)
    .then(result=>{
        res.redirect('/cart');
    }).catch(err=>{
        console.log(err);
    })
}

exports.getCheckOut = (req,res,next)=>{
    req.user
    .populate('cart.items.productId')
    .execPopulate()
    .then(user=>{
        const products = user.cart.items;
        let totalPrice = 0;
        products.forEach(p=>{
            totalPrice+=p.quantity*p.productId.price;
        })
        res.render('shop/checkout',
        {
            pageTitle:'Checkout',
            path:'/checkout',
            products:products,
            totalPrice:totalPrice
        });  
    });
}

exports.postOrder = ((req,res,next)=>{
    req.user
        .populate('cart.items.productId')
        .execPopulate()
        .then(user=>{
            const products = user.cart.items;
            const order = new Order({
                user:{
                    email:req.user.email,
                    userId:req.user
                },
                products:products.map(i=>{
                    return {
                        quantity:i.quantity,
                        product:{...i.productId._doc}
                    }
                })
            });
            return order.save();
        }).then(result=>{
            return req.user.clearCart();
        }).then(result=>{
            res.redirect('/orders')
        })
        .catch(err=>{
            return errorController.handleError(err,next);
        });
})



exports.getOrders = (req,res,next) => {
    Order.find({"user.userId" : req.user._id})
        .then(orders=>{
        res.render
        ('shop/orders',
        {
            pageTitle:'Your Orders',
            path:'/orders',
            orders:orders,
        }
        );
    })
        
}

exports.getInvoice = (req,res,next)=>{
    const orderId = req.params.orderId;
    Order.findById(orderId)
    .then(order=>{
        if(!order){
            return next(new Error('Order not found.'));
        }
        if(order.user.userId.toString() !== req.user._id.toString()){
            return next(new Error('Unauthorized.'));
        }
        const invoiceName = 'invoice-'+ orderId + '.pdf';
        const invoicePath = path.join('data','invoices',invoiceName);
        res.setHeader('Content-Type','application/pdf');
        //res.setHeader('Content-Disposition','inline; filename="' + invoiceName + '"');
        const pdfDoc = new PDFDocument();
        pdfDoc.pipe(fs.createWriteStream(invoicePath));
        pdfDoc.pipe(res);
        pdfDoc.fontSize(26).text('Invoice',{
            underline:true
        });
        pdfDoc.text('----------------------');
        let totalPrice = 0;
        order.products.forEach(prod=>{
            pdfDoc.fontSize(14).text(prod.product.title +' - ' + prod.quantity + ' x ' + '$' + prod.product.price);
            totalPrice = totalPrice + prod.quantity * prod.product.price;
        });
        pdfDoc.text('----------------------');
        pdfDoc.fontSize(20).text('Total Price: $' + totalPrice);
        pdfDoc.end();
        
    }).catch(err=>{
        next(err);
    })
}

