const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const productSchema = new Schema({
    title:{
        type:String,
        required:true
    },
    imageUrl:{
        type:String,
        required:true
    },
    price:{
        type:Number,
        required:true
    },
    description:{
        type:String,
        required:true
    },
    userId:{
        type:Schema.Types.ObjectId,
        ref:'User',
        required:true
    }
});
module.exports = mongoose.model('Product',productSchema);
/*
const mongodb = require('mongodb');
const getDb = require('../util/database').getDb;

class Product{
    constructor(_id,userId,title,imageUrl,price,description){
        this._id = _id? mongodb.ObjectID(_id):null;  
        this.userId = userId;      
        this.title = title;
        this.imageUrl = imageUrl;
        this.price = price;
        this.description = description;
    }
    save(){
        const db = getDb();
        let db0;

        if(this._id){
            db0 = db.collection('products').updateOne({_id : this._id},{$set:this});
        }else{
            db0 = db.collection('products').insertOne(this);
        }
        return db0
        .then(result=>{
        })
        .catch(err=>{
            console.log(err);
        });
    }

    static fetchAll(){
        const db = getDb();
        return db.collection('products')
        .find()
        .toArray()
        .then(products=>{
            return products;
        })
        .catch(err=>{
            console.log(err);
        })
    }
    static findById(productId){
        const db= getDb();
        return db.collection('products').find({_id : new mongodb.ObjectId(productId)})
        .next()
        .then(product =>{
            return product;
        }).catch(err=>{
            console.log(err);
        });
    }
    static deleteById(productId){
        const db = getDb();
        return db.collection('products').deleteOne({_id:new mongodb.ObjectID(productId)});
    }
}


module.exports = Product;
*/